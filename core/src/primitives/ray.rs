use euclid::{Point2D, Vector2D};

#[derive(Debug, Copy, Clone, PartialEq)]
#[cfg_attr(feature = "serde", derive(serde::Serialize, serde::Deserialize))]
pub struct Ray<Space> {
    origin: Point2D<f64, Space>,
    direction: Vector2D<f64, Space>,
}

impl<Space> Ray<Space> {
    pub const fn new(
        origin: Point2D<f64, Space>,
        direction: Vector2D<f64, Space>,
    ) -> Self {
        Ray { origin, direction }
    }

    pub const fn origin(self) -> Point2D<f64, Space> { self.origin }

    pub const fn direction(self) -> Vector2D<f64, Space> { self.direction }
}
