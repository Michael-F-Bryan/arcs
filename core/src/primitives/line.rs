use euclid::{Length, Point2D, Vector2D};

use crate::Orientation;

/// A line connecting [`Line::start`] to [`Line::end`].
#[derive(Debug, Default, PartialEq)]
#[cfg_attr(feature = "serde", derive(serde::Serialize, serde::Deserialize))]
pub struct Line<S> {
    /// The [`Line`]'s starting point.
    pub start: Point2D<f64, S>,
    /// The [`Line`]'s ending point.
    pub end: Point2D<f64, S>,
}

impl<S> Line<S> {
    /// Create a new [`Line`].
    pub const fn new(start: Point2D<f64, S>, end: Point2D<f64, S>) -> Self {
        Line { start, end }
    }

    /// The displacement vector from [`Line::start`] to [`Line::end`].
    pub fn displacement(&self) -> Vector2D<f64, S> { self.end - self.start }

    /// The [`Line::displacement()`], normalised to a unit vector.
    pub fn direction(&self) -> Vector2D<f64, S> {
        self.displacement().normalize()
    }

    /// The [`Line`]'s length.
    pub fn length(self) -> f64 { self.displacement().length() }

    ///  How close would the [`Point2D`] get if this line were extended
    /// forever?
    ///
    /// See also [*Distance from a point to a line*][wiki] on Wikipedia.
    ///
    /// [wiki]: https://en.wikipedia.org/wiki/Distance_from_a_point_to_a_line
    pub fn perpendicular_distance_to(
        self,
        point: Point2D<f64, S>,
    ) -> Length<f64, S> {
        const SOME_SMALL_NUMBER: f64 = std::f64::EPSILON * 100.0;

        let side_a = self.start - point;
        let side_b = self.end - point;
        let area = Vector2D::cross(side_a, side_b) / 2.0;

        // area = base * height / 2
        let base_length = self.length();

        Length::new(if base_length.abs() < SOME_SMALL_NUMBER {
            side_a.length()
        } else {
            area.abs() * 2.0 / base_length
        })
    }

    pub fn intersects_with(self, other: Self) -> bool {
        // https://www.geeksforgeeks.org/check-if-two-given-line-segments-intersect/

        // Find the four orientations needed for general and
        // special cases
        let Line { start: p1, end: q1 } = self;
        let Line { start: p2, end: q2 } = other;
        let o1 = Orientation::of(p1, q1, p2);
        let o2 = Orientation::of(p1, q1, q2);
        let o3 = Orientation::of(p2, q2, p1);
        let o4 = Orientation::of(p2, q2, q1);

        // The general case
        (o1 != o2 && o3 != o4) ||
        // p1, q1 and p2 are colinear and p2 lies on segment p1q1
        (o1 == Orientation::Collinear && on_segment(p1, p2, q1))  ||
        // p1, q1 and q2 are colinear and q2 lies on segment p1q1
        (o2 == Orientation::Collinear && on_segment(p1, q2, q1)) ||
        // p2, q2 and p1 are colinear and p1 lies on segment p2q2
        (o3 == Orientation::Collinear && on_segment(p2, p1, q2)) ||
        // p2, q2 and q1 are colinear and q1 lies on segment p2q2
        (o4 == Orientation::Collinear && on_segment(p2, q1, q2))
    }
}

fn on_segment<S>(
    a: Point2D<f64, S>,
    b: Point2D<f64, S>,
    c: Point2D<f64, S>,
) -> bool {
    b.x <= f64::max(a.x, c.x)
        && b.x >= f64::min(a.x, c.x)
        && b.y <= f64::max(a.y, c.y)
        && b.y >= f64::min(a.y, c.y)
}

impl<S> Copy for Line<S> {}

impl<S> Clone for Line<S> {
    fn clone(&self) -> Self { *self }
}

#[cfg(test)]
mod tests {
    use super::*;

    type Point = euclid::default::Point2D<f64>;
    type Vector = euclid::default::Vector2D<f64>;

    #[test]
    fn calculate_length() {
        let start = Point::new(1.0, 1.0);
        let displacement = Vector::new(3.0, 4.0);
        let v = Line::new(start, start + displacement);

        assert_eq!(v.length(), 5.0);
        assert_eq!(v.displacement(), displacement);
    }

    #[test]
    fn two_intersecting_lines() {
        let l1 = Line::new(Point::new(4.0, 0.0), Point::new(6.0, 10.0));
        let l2 = Line::new(Point::new(0.0, 3.0), Point::new(10.0, 7.0));
        // Intersects at (5.0, 5.0)

        assert!(l1.intersects_with(l2));
    }

    #[test]
    fn non_intersecting_lines() {
        let l1 = Line::new(Point::new(0.0, 0.0), Point::new(1.0, 1.0));
        let l2 = Line::new(Point::new(1.0, 2.0), Point::new(4.0, 5.0));

        assert!(!l1.intersects_with(l2));
    }
}
