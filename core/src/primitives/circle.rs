use euclid::{Length, Point2D};

#[derive(Debug, Copy, Clone, PartialEq)]
#[cfg_attr(feature = "serde", derive(serde::Serialize, serde::Deserialize))]
pub struct Circle<Space> {
    pub centre: Point2D<f64, Space>,
    pub radius: Length<f64, Space>,
}

impl<Space> Circle<Space> {
    pub const fn new(
        centre: Point2D<f64, Space>,
        radius: Length<f64, Space>,
    ) -> Self {
        Circle { centre, radius }
    }
}
