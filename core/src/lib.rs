//! Core primitives and algorithms used by the arcs CAD engine.
//!
//! # Coordinate Systems
//!
//! There are a couple coordinate systems going on in a CAD application,
//! with the most important being:
//!
//! - [*Canvas Space:*][CanvasSpace] the location of a pixel on the HTML canvas
//!   (graphical coordinates)
//! - [*Drawing Space:*][DrawingSpace] a location in the drawing (cartesian
//!   coordinates)
//!
//! # Feature Flags
//!
//! This crate uses conditional compilation to let you pull in only the
//! functionality you need. The feature flags currently available are:
//!
//! - `serde-1` - for serialization and deserialization with `serde`

#![deny(
    missing_debug_implementations,
    missing_copy_implementations,
    // missing_docs,
    rust_2018_idioms,
    rustdoc::broken_intra_doc_links,
    future_incompatible
)]
#![forbid(unsafe_code)]

pub mod algorithms;
mod orientation;
mod overridable;
pub mod primitives;

pub use orientation::{centre_of_three_points, Orientation};
pub use overridable::Overridable;

/// A strongly-typed angle, useful for dealing with the pesky modular arithmetic
/// normally associated with circles and angles.
pub type Angle = euclid::Angle<f64>;
pub type Point = euclid::Point2D<f64, DrawingSpace>;
pub type Length = euclid::Length<f64, DrawingSpace>;
pub type Vector = euclid::Vector2D<f64, DrawingSpace>;

#[derive(Debug, Copy, Clone, PartialEq, Eq, PartialOrd, Ord, Hash)]
#[cfg_attr(feature = "serde", derive(serde::Serialize, serde::Deserialize))]
pub enum CanvasSpace {}

impl Default for CanvasSpace {
    fn default() -> Self { unreachable!() }
}

#[derive(Debug, Copy, Clone, PartialEq, Eq, PartialOrd, Ord, Hash)]
#[cfg_attr(feature = "serde", derive(serde::Serialize, serde::Deserialize))]
pub enum DrawingSpace {}

impl Default for DrawingSpace {
    fn default() -> Self { unreachable!() }
}
