/// A component which may be overridden individually, falling back to the
/// inherited value, otherwise.
#[derive(Debug, Clone)]
#[cfg_attr(
    feature = "serde-1",
    derive(serde::Serialize, serde::Deserialize),
    serde(tag = "type")
)]
pub enum Overridable<T> {
    Overridden(T),
    Inherited,
}

impl<T> Overridable<T> {
    pub fn realise<'a>(&'a self, parent: &'a T) -> &'a T {
        match self {
            Overridable::Overridden(s) => s,
            Overridable::Inherited => parent,
        }
    }
}

impl<T> Default for Overridable<T> {
    fn default() -> Self { Overridable::Inherited }
}

impl<T> From<T> for Overridable<T> {
    fn from(item: T) -> Self { Overridable::Overridden(item) }
}
