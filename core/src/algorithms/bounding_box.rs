use crate::primitives::{Arc, Line};
use euclid::{Angle, Point2D, Rect, Size2D};

/// Calculate an axis-aligned bounding box around the item.
pub trait Bounded<S> {
    /// Calculate the approximate location this object is located in.
    fn bounding_box(&self) -> Rect<f64, S>;
}

impl<'a, S, B: Bounded<S> + ?Sized> Bounded<S> for &'a B {
    fn bounding_box(&self) -> Rect<f64, S> { (*self).bounding_box() }
}

impl<S> Bounded<S> for Point2D<f64, S> {
    fn bounding_box(&self) -> Rect<f64, S> { Rect::new(*self, Size2D::zero()) }
}

impl<S> Bounded<S> for Rect<f64, S> {
    fn bounding_box(&self) -> Rect<f64, S> { *self }
}

impl<S> Bounded<S> for Line<S> {
    fn bounding_box(&self) -> Rect<f64, S> {
        Rect::from_points(&[self.start, self.end])
    }
}

impl<S> Bounded<S> for Arc<S> {
    fn bounding_box(&self) -> Rect<f64, S> {
        let (x, y) = self.centre().to_tuple();
        let r = self.radius();

        let mut points = vec![self.start(), self.end()];

        if self.contains_angle(Angle::zero()) {
            let right = Point2D::new(x + r, y);
            points.push(right);
        }
        if self.contains_angle(Angle::frac_pi_2()) {
            let top = Point2D::new(x, y + r);
            points.push(top);
        }
        if self.contains_angle(Angle::pi()) {
            let left = Point2D::new(x - r, y);
            points.push(left);
        }
        if self.contains_angle(Angle::pi() + Angle::frac_pi_2()) {
            let bottom = Point2D::new(x, y - r);
            points.push(bottom);
        }

        Rect::from_points(points)
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use euclid::default::Point2D;

    #[test]
    fn bounding_box_around_line() {
        let start = Point2D::<f64>::zero();
        let end = Point2D::<f64>::new(3.0, 4.0);
        let line = Line::new(start, end);

        let bounds = line.bounding_box();

        assert_eq!(bounds.width(), 3.0);
        assert_eq!(bounds.height(), 4.0);
        assert_eq!(bounds.min_x(), start.x);
        assert_eq!(bounds.min_y(), start.y);
        assert_eq!(bounds.max_x(), end.x);
        assert_eq!(bounds.max_y(), end.y);
    }
}
