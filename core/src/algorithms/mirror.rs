use crate::primitives::Line;
use euclid::{Point2D, Scale, Vector2D};

pub trait Mirror<Space> {
    fn mirror(&mut self, line: Line<Space>);

    fn mirrored(&self, line: Line<Space>) -> Self
    where
        Self: Clone,
    {
        debug_assert!(
            line.length() > 0.0,
            "Can't mirror about a zero-length vector"
        );

        let mut mirrored = self.clone();
        mirrored.mirror(line);
        mirrored
    }
}

impl<Space> Mirror<Space> for Point2D<f64, Space> {
    fn mirror(&mut self, line: Line<Space>) {
        // We implement mirroring by projecting the point onto the line, giving
        // us the closest location between line and point.

        let start = line.start;
        let displacement = line.displacement();

        let t = Vector2D::dot(*self - start, displacement)
            / (line.length() * line.length());

        let closest_point =
            start + Scale::new(t).transform_vector(displacement);

        // Then add 2x the displacement to get the equivalent point on the other
        // side of the line.

        *self += (closest_point - *self) * 2.0;
    }
}

impl<Space> Mirror<Space> for Line<Space> {
    fn mirror(&mut self, line: Line<Space>) {
        self.start.mirror(line);
        self.end.mirror(line);
    }
}

#[cfg(test)]
mod tests {
    use euclid::{approxeq::ApproxEq, default::Point2D};

    use super::*;

    #[test]
    fn mirror_a_point_diagonally() {
        let p = Point2D::new(1.0, 0.0);
        let line = Line::new(Point2D::new(0.0, 0.0), Point2D::new(10.0, 10.0));

        let got = p.mirrored(line);

        assert!(got.approx_eq_eps(
            &Point2D::new(0.0, 1.0),
            &Point2D::new(0.001, 0.001)
        ));
    }

    #[test]
    fn mirror_a_point_about_y_axis() {
        let p = Point2D::new(1.0, 0.0);
        let line = Line::new(Point2D::new(0.0, 0.0), Point2D::new(0.0, 10.0));

        let got = p.mirrored(line);

        assert_eq!(got, Point2D::new(-1.0, 0.0));
    }

    #[test]
    fn mirror_a_point_about_x_axis() {
        let p = Point2D::new(0.0, 10.0);
        let line = Line::new(Point2D::new(0.0, 0.0), Point2D::new(10.0, 0.0));

        let got = p.mirrored(line);

        assert_eq!(got, Point2D::new(0.0, -10.0));
    }
}
