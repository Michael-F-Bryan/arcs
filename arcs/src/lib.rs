//! A Rust CAD System.
//!
//! # Feature Flags
//!
//! This crate uses conditional compilation to let you pull in only the
//! functionality you need. The feature flags currently available are:
//!
//! - `serde-1` - for serialization and deserialization with `serde`

#![deny(missing_docs, rustdoc::broken_intra_doc_links)]

#[doc(inline)]
pub use arcs_render as rendering;

#[doc(inline)]
pub use arcs_core::{primitives, CanvasSpace, DrawingSpace, Overridable};

/// Useful algorithms and functionality for manipulating graphical objects.
pub mod algorithms {
    #[doc(inline)]
    pub use arcs_core::algorithms::*;
    #[doc(inline)]
    pub use arcs_core::{
        centre_of_three_points, Angle, Length, Orientation, Point,
    };
}
