use arcs_core::{primitives::Line, Overridable};
use arcs_render::{
    Background, Dimension, GraphicalObject, LineStyle, NormalStyles,
    SelectedStyles, Viewport,
};
use euclid::{Point2D, Scale, Size2D};
use legion::{Resources, World};
use piet::{Color, RenderContext};

macro_rules! snapshot_test {
    ($name:ident, |$world:ident : &mut World| $body:expr) => {
        #[test]
        fn $name() {
            let mut $world = World::default();
            let mut resources = Resources::default();
            arcs_render::init_resources(&mut resources, &mut $world);

            let canvas_dimensions = Size2D::new(400.0, 300.0);

            let viewport: Viewport = $body;
            let screen = $world.push((
                viewport,
                NormalStyles::default(),
                SelectedStyles::default(),
                Background::default(),
            ));

            let mut ctx = piet_svg::RenderContext::new();
            arcs_render::render(
                screen,
                &$world,
                &resources,
                canvas_dimensions,
                &mut ctx,
            );
            ctx.finish().unwrap();

            let mut svg = Vec::new();
            ctx.write(&mut svg).unwrap();
            let svg = String::from_utf8(svg).unwrap();

            insta::assert_snapshot!(stringify!($name), &svg);
        }
    };
}

snapshot_test! {
    single_line,
    |world: &mut World| {
        world.push((
            GraphicalObject::Line(Line {
                start: Point2D::new(0.0, 0.0),
                end: Point2D::new(100.0, 0.0),
            }),
            Overridable::Overridden(LineStyle {
                stroke: Color::BLACK,
                width: Dimension::Pixels(1.0),
            }),
        ));

        Viewport {
            centre: Point2D::new(50.0, 0.0),
            pixels_per_drawing_unit: Scale::new(1.0),
        }
    }
}
