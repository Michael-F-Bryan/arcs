use std::ops::{Deref, DerefMut};

use crate::Dimension;
use arcs_core::{
    algorithms::{Approximate, Bounded, Translate},
    DrawingSpace, Vector,
};
use euclid::{Point2D, Rect};
use piet::Color;

pub const DEFAULT_COLOUR: Color = Color::BLACK;
pub const DEFAULT_BACKGROUND_COLOUR: Color = Color::WHITE;
pub const DEFAULT_SELECTED_COLOUR: Color = Color::BLUE;

#[derive(Debug, Clone)]
#[cfg_attr(feature = "serde-1", derive(serde::Serialize, serde::Deserialize))]
pub struct StyleGroup {
    pub point: PointStyle,
    pub line: LineStyle,
}

/// Styles used when rendering objects that *aren't* [`Selected`].
#[derive(Debug, Clone)]
#[cfg_attr(feature = "serde-1", derive(serde::Serialize, serde::Deserialize))]
pub struct NormalStyles(pub StyleGroup);

impl Deref for NormalStyles {
    type Target = StyleGroup;

    fn deref(&self) -> &Self::Target { &self.0 }
}

impl DerefMut for NormalStyles {
    fn deref_mut(&mut self) -> &mut Self::Target { &mut self.0 }
}

impl Default for NormalStyles {
    fn default() -> Self {
        NormalStyles(StyleGroup {
            point: PointStyle {
                colour: DEFAULT_COLOUR,
                ..Default::default()
            },
            line: LineStyle {
                stroke: DEFAULT_COLOUR,
                ..Default::default()
            },
        })
    }
}

/// Styles used when rendering [`Selected`] objects.
#[derive(Debug, Clone)]
#[cfg_attr(feature = "serde-1", derive(serde::Serialize, serde::Deserialize))]
pub struct SelectedStyles(pub StyleGroup);

impl Deref for SelectedStyles {
    type Target = StyleGroup;

    fn deref(&self) -> &Self::Target { &self.0 }
}

impl DerefMut for SelectedStyles {
    fn deref_mut(&mut self) -> &mut Self::Target { &mut self.0 }
}

impl Default for SelectedStyles {
    fn default() -> Self {
        // we want to override *just* the colour from our normal styles
        let NormalStyles(StyleGroup { point, line }) = NormalStyles::default();

        SelectedStyles(StyleGroup {
            point: PointStyle {
                colour: DEFAULT_SELECTED_COLOUR,
                ..point
            },
            line: LineStyle {
                stroke: DEFAULT_SELECTED_COLOUR,
                ..line
            },
        })
    }
}

#[derive(Debug, Clone)]
#[cfg_attr(feature = "serde-1", derive(serde::Serialize, serde::Deserialize))]
pub struct PointStyle {
    #[cfg_attr(feature = "serde-1", serde(with = "color"))]
    pub colour: Color,
    pub radius: Dimension,
}

impl Default for PointStyle {
    fn default() -> PointStyle {
        PointStyle {
            colour: DEFAULT_COLOUR,
            radius: Dimension::default(),
        }
    }
}

#[derive(Debug, Clone)]
#[cfg_attr(feature = "serde-1", derive(serde::Serialize, serde::Deserialize))]
pub struct LineStyle {
    #[cfg_attr(feature = "serde-1", serde(with = "color"))]
    pub stroke: Color,
    pub width: Dimension,
}

impl Default for LineStyle {
    fn default() -> LineStyle {
        LineStyle {
            stroke: DEFAULT_COLOUR,
            width: Dimension::default(),
        }
    }
}

/// Options for drawing rulers around the outside of the screen.
#[derive(Debug, Copy, Clone, PartialEq)]
#[cfg_attr(feature = "serde-1", derive(serde::Serialize, serde::Deserialize))]
pub struct Rulers {}

/// An empty component used to mark an entity as selected.
#[derive(Debug, Copy, Clone, Default, PartialEq)]
#[cfg_attr(feature = "serde-1", derive(serde::Serialize, serde::Deserialize))]
pub struct Selected;

/// A logical grouping of data, assembled as though each [`Layer`] were laid out
/// on transparent acetate overlays.
#[derive(Debug, Copy, Clone, PartialEq, Eq, Hash)]
#[cfg_attr(feature = "serde-1", derive(serde::Serialize, serde::Deserialize))]
pub struct Layer {
    /// The z-coordinate. Lower z-levels will be drawn above higher z-levels.
    pub z_level: usize,
    /// Should entities on this layer be displayed?
    pub visible: bool,
}

impl Default for Layer {
    fn default() -> Layer {
        Layer {
            z_level: 0,
            visible: true,
        }
    }
}

#[derive(Debug, Clone, PartialEq)]
#[cfg_attr(feature = "serde-1", derive(serde::Serialize, serde::Deserialize))]
pub struct Background {
    #[cfg_attr(feature = "serde-1", serde(with = "color"))]
    pub colour: Color,
}

impl Default for Background {
    fn default() -> Self {
        Background {
            colour: DEFAULT_BACKGROUND_COLOUR,
        }
    }
}

#[derive(Debug, Clone, PartialEq)]
#[cfg_attr(feature = "serde-1", derive(serde::Serialize, serde::Deserialize))]
pub enum GraphicalObject {
    Line(arcs_core::primitives::Line<DrawingSpace>),
    Rectangle(Rect<f64, DrawingSpace>),
}

impl From<arcs_core::primitives::Line<DrawingSpace>> for GraphicalObject {
    fn from(line: arcs_core::primitives::Line<DrawingSpace>) -> Self {
        GraphicalObject::Line(line)
    }
}

impl From<Rect<f64, DrawingSpace>> for GraphicalObject {
    fn from(rect: Rect<f64, DrawingSpace>) -> Self {
        GraphicalObject::Rectangle(rect)
    }
}

impl Bounded<DrawingSpace> for GraphicalObject {
    fn bounding_box(&self) -> Rect<f64, DrawingSpace> {
        match self {
            GraphicalObject::Line(line) => line.bounding_box(),
            GraphicalObject::Rectangle(rect) => rect.bounding_box(),
        }
    }
}

impl Translate<DrawingSpace> for GraphicalObject {
    fn translate(&mut self, delta: Vector) {
        match self {
            GraphicalObject::Line(line) => line.translate(delta),
            GraphicalObject::Rectangle(rect) => rect.translate(delta),
        }
    }
}

impl Approximate<DrawingSpace> for GraphicalObject {
    type IntoIter = Vec<Point2D<f64, DrawingSpace>>;

    fn approximate(&self, tolerance: f64) -> Self::IntoIter {
        match self {
            GraphicalObject::Line(line) => {
                line.approximate(tolerance).into_iter().collect()
            },
            GraphicalObject::Rectangle(rect) => {
                rect.approximate(tolerance).into_iter().collect()
            },
        }
    }
}

#[cfg(feature = "serde-1")]
mod color {
    use piet::Color;
    use serde::{Deserialize, Deserializer, Serialize, Serializer};

    pub fn serialize<S: Serializer>(
        c: &Color,
        ser: S,
    ) -> Result<S::Ok, S::Error> {
        c.as_rgba_u32().serialize(ser)
    }

    pub fn deserialize<'de, D: Deserializer<'de>>(
        de: D,
    ) -> Result<Color, D::Error> {
        u32::deserialize(de).map(Color::from_rgba32_u32)
    }
}
