//! Data structures and algorithms used to render `arcs` drawings to the screen.
//!
//! # Coordinate Systems
//!
//! There are a couple coordinate systems going on in a CAD application,
//! with the most important being:
//!
//! - [*Canvas Space:*][arcs_core::CanvasSpace] the location of a pixel on the
//!   HTML canvas (graphical coordinates)
//! - [*Drawing Space:*][arcs_core::DrawingSpace] a location in the drawing
//!   (cartesian coordinates)
//!
//! To convert between *Canvas* and *Drawing* coordinates we need to:
//!
//! - Flip vertically about the middle of the canvas
//! - Translate from the centre of the canvas to the `centre` of the
//!   [`Viewport`]
//! - Scale to respect [`Viewport::pixels_per_drawing_unit`]
//!
//! All geometry types are based on the primitives from [`euclid`], which lets
//! us use type "tags" to make sure values from different coordinate systems
//! don't get mixed up.
//!
//! [`Viewport::transform()`] will give you a
//! transform matrix that handles the conversion correctly.

mod components;
mod dimension;
mod passes;
mod resources;
mod viewport;

pub use components::*;
pub use dimension::Dimension;
pub use passes::render;
pub use resources::*;
pub use viewport::Viewport;
