use crate::{passes::RenderState, Rulers};
use legion::IntoQuery;
use piet::RenderContext;

/// Draw rulers along the outside of the window.
///
/// Rulers will only be drawn if the [`RenderState::screen`] has a [`Rulers`]
/// component.
pub(crate) fn render<R: RenderContext>(state: &mut RenderState<R>) {
    let mut query = <Option<&Rulers>>::query();

    if let Ok(Some(_)) = query.get(state.world, state.screen) {
        todo!()
    }
}
