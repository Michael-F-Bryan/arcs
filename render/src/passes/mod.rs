//! Rendering passes.

mod background;
mod objects;
mod rulers;

use arcs_core::{algorithms::Approximate, CanvasSpace, DrawingSpace};
use euclid::{Rect, Size2D, Transform2D};
use legion::{systems::Fetch, Entity, IntoQuery, Resources, World};
use piet::RenderContext;

use crate::{
    components::{NormalStyles, SelectedStyles},
    Background, LocationCache, Viewport,
};

/// Render the visible parts of a drawing.
///
/// The `screen` entity must have the following components:
///
/// - [`Viewport`]
/// - [`NormalStyles`]
/// - [`SelectedStyles`]
pub fn render<R>(
    screen: Entity,
    world: &World,
    resources: &Resources,
    canvas_dimensions: Size2D<f64, CanvasSpace>,
    ctx: &mut R,
) where
    R: RenderContext,
{
    let canvas_extents = Rect::from_size(canvas_dimensions);
    let mut state =
        RenderState::new(screen, world, resources, canvas_extents, ctx);

    background::render(&mut state);
    objects::render(&mut state);
    rulers::render(&mut state);
}

pub(crate) struct RenderState<'ctx, 'world, R> {
    pub ctx: &'ctx mut R,
    pub transform: Transform2D<f64, DrawingSpace, CanvasSpace>,
    pub screen: Entity,
    pub world: &'world World,
    pub normal_styles: &'world NormalStyles,
    pub selected_styles: &'world SelectedStyles,
    pub viewport: &'world Viewport,
    pub background: &'world Background,
    pub canvas_extents: Rect<f64, CanvasSpace>,
    pub visible_area: Rect<f64, DrawingSpace>,
    pub location_cache: Fetch<'world, LocationCache>,
}

macro_rules! query {
    ($world:ident . get($entity:expr) as ($($type:ty),* $(,)?)) => {
        match <( $( &$type, )* )>::query().get($world, $entity) {
            Ok(values) => values,
            Err(e) => {
                let required = [
                    $( std::any::type_name::<$type>() ),*
                ];

                panic!("Unable to find the required components: {}. Expected {}", e, required.join(", "));
            }
        }
    };
}

impl<'ctx, 'world, R: RenderContext> RenderState<'ctx, 'world, R> {
    fn new(
        screen: Entity,
        world: &'world World,
        resources: &'world Resources,
        canvas_extents: Rect<f64, CanvasSpace>,
        ctx: &'ctx mut R,
    ) -> Self {
        let (normal_styles, selected_styles, viewport, background) =
            query!(world.get(screen)
                as (NormalStyles, SelectedStyles, Viewport, Background,));

        let transform = viewport.transform(canvas_extents.size);

        let corners = canvas_extents
            .approximate(0.0)
            .into_iter()
            .map(|c| transform.transform_point(c));
        let visible_area = Rect::from_points(corners);

        let location_cache = resources
            .get()
            .expect("Couldn't get the LocationCache resource");

        RenderState {
            ctx,
            viewport,
            world,
            screen,
            canvas_extents,
            normal_styles,
            selected_styles,
            background,
            location_cache,
            visible_area,
            transform: transform.inverse()
                .expect("Unable to get the inverse viewport transform, do you have a zero dimension somewhere?"),
        }
    }
}
