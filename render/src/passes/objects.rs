use crate::{passes::RenderState, GraphicalObject, LineStyle, Selected};
use arcs_core::{primitives::Line, DrawingSpace, Overridable};
use euclid::Rect;
use legion::IntoQuery;
use piet::RenderContext;

/// Render all visible graphical objects.
pub(crate) fn render<R: RenderContext>(state: &mut RenderState<R>) {
    let mut lines = <(
        &GraphicalObject,
        &Overridable<LineStyle>,
        Option<&Selected>,
    )>::query();
    let selected_style = &state.selected_styles.line;

    for (obj, style, selected) in lines.iter(state.world) {
        let style = if selected.is_some() {
            selected_style
        } else {
            style.realise(&state.normal_styles.line)
        };

        match obj {
            GraphicalObject::Line(line) => render_line(state, line, style),
            GraphicalObject::Rectangle(rect) => {
                render_rectangle(state, rect, style)
            },
        }
    }
}

fn render_rectangle<R: RenderContext>(
    state: &mut RenderState<R>,
    rect: &Rect<f64, DrawingSpace>,
    style: &LineStyle,
) {
    let Rect { origin, size } = rect;
    let size = state.viewport.pixels_per_drawing_unit.transform_size(*size);
    let centre = state.transform.transform_point(*origin);

    let rect = Rect::new(centre, size);

    if state.canvas_extents.intersects(&rect) {
        let LineStyle { stroke, width } = style;
        let brush = state.ctx.solid_brush(stroke.clone());
        let width = width.in_pixels(state.viewport.pixels_per_drawing_unit);

        let shape = piet::kurbo::Rect::new(
            rect.min_x(),
            rect.min_y(),
            rect.max_x(),
            rect.max_y(),
        );
        state.ctx.stroke(shape, &brush, width)
    }
}

fn render_line<R: RenderContext>(
    state: &mut RenderState<R>,
    line: &Line<DrawingSpace>,
    style: &LineStyle,
) {
    let start = state.transform.transform_point(line.start);
    let end = state.transform.transform_point(line.end);
    let bounds = Rect::from_points([start, end].iter().copied());

    if state.canvas_extents.intersects(&bounds) {
        let shape = piet::kurbo::Line::new(start.to_tuple(), end.to_tuple());
        let LineStyle { stroke, width } = style;
        let brush = state.ctx.solid_brush(stroke.clone());
        let width = width.in_pixels(state.viewport.pixels_per_drawing_unit);

        state.ctx.stroke(shape, &brush, width);
    }
}
