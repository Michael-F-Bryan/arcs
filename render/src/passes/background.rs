use crate::passes::RenderState;
use piet::RenderContext;

pub(crate) fn render<R: RenderContext>(state: &mut RenderState<R>) {
    state.ctx.clear(state.background.colour.clone());
}
