use crate::GraphicalObject;
use arcs_core::{
    algorithms::{Approximate, Bounded},
    primitives::Line,
    DrawingSpace,
};
use crossbeam::channel::{self, Receiver, Sender};
use euclid::Rect;
use legion::{
    systems::Builder,
    world::{Event, EventSender, SubWorld},
    Entity, IntoQuery, Resources, World,
};

pub fn register_maintenance_systems(builder: &mut Builder) {
    builder.add_system(update_location_cache_system()).flush();
}

pub fn init_resources(res: &mut Resources, world: &mut World) {
    res.insert(LocationCache::default());

    // set up a subscription so the update_location_cache system is notified
    // whenever something moves
    let (sender, receiver) = channel::unbounded();
    world.subscribe(
        LocationChangesSender(sender),
        legion::component::<GraphicalObject>(),
    );
    res.insert(LocationChangesReceiver(receiver));
}

/// A cache for looking up entities by their location in the drawing.
#[derive(Debug, Default, Clone, PartialEq)]
pub struct LocationCache {
    regions: Vec<(Entity, Rect<f64, DrawingSpace>)>,
}

impl LocationCache {
    pub fn is_empty(&self) -> bool { self.regions.is_empty() }

    /// Find all entities who's bounding box overlaps with the target region.
    pub fn iter_overlapping(
        &self,
        target_region: Rect<f64, DrawingSpace>,
    ) -> impl Iterator<Item = (Entity, Rect<f64, DrawingSpace>)> + '_ {
        self.regions
            .iter()
            .filter(move |(_, region)| target_region.intersects(region))
            .copied()
    }

    pub fn items_within_region<'a>(
        &'a self,
        world: &'a World,
        target_region: Rect<f64, DrawingSpace>,
    ) -> impl Iterator<Item = (Entity, &'a GraphicalObject)> + 'a {
        self.iter_overlapping(target_region)
            .filter_map(move |(ent, _)| {
                let obj = <&GraphicalObject>::query().get(world, ent).ok()?;

                if within_region(obj, target_region) {
                    Some((ent, obj))
                } else {
                    None
                }
            })
    }

    pub fn update(&mut self, entity: Entity, bounds: Rect<f64, DrawingSpace>) {
        match self.regions.iter().position(|(e, _)| *e == entity) {
            Some(ix) => self.regions[ix].1 = bounds,
            None => self.regions.push((entity, bounds)),
        }
    }

    pub fn remove(&mut self, entity: Entity) {
        if let Some(ix) = self.regions.iter().position(|(e, _)| *e == entity) {
            self.regions.swap_remove(ix);
        }
    }
}

fn within_region(
    obj: &GraphicalObject,
    region: Rect<f64, DrawingSpace>,
) -> bool {
    tracing::info!(?obj, ?region, "Checking if object is within region");

    let tolerance = f64::hypot(region.width(), region.height()) / 100.0;
    let edges: Vec<_> = region
        .approximate(1.0)
        .windows(2)
        .map(|w| Line::new(w[0], w[1]))
        .collect();

    for window in obj.approximate(tolerance).windows(2) {
        let segment = Line::new(window[0], window[1]);
        let span = tracing::info_span!("window", ?segment);
        let _guard = span.enter();

        if !region.intersects(&segment.bounding_box()) {
            // this line is totally outside the region
            continue;
        }

        // the line segment's bounding box overlaps, but is it actually in the
        // region?
        if region.contains(segment.start) || region.contains(segment.end) {
            tracing::info!("Contains!");
            return true;
        }

        // otherwise the line segment may cut across one of the borders.
        for edge in &edges {
            tracing::info!(?edge, "Intersection test");
            if edge.intersects_with(segment) {
                tracing::info!("Intersects!");
                return true;
            }
        }
    }

    false
}

#[legion::system]
#[read_component(GraphicalObject)]
#[tracing::instrument(skip(world, cache, changes))]
fn update_location_cache(
    world: &SubWorld,
    #[resource] cache: &mut LocationCache,
    #[resource] changes: &mut LocationChangesReceiver,
) {
    // handle new/deleted graphical entities
    let mut query = <&GraphicalObject>::query();

    while let Ok(event) = changes.0.try_recv() {
        match event {
            Event::EntityInserted(e, _) => {
                if let Ok(obj) = query.get(world, e) {
                    cache.update(e, obj.bounding_box());
                }
            },
            Event::EntityRemoved(e, _) => {
                cache.remove(e);
            },
            _ => continue,
        };
    }

    // then we can handle objects which were mutated in place
    let mut query = <(Entity, &GraphicalObject)>::query()
        .filter(legion::maybe_changed::<GraphicalObject>());

    for (ent, obj) in query.iter(world) {
        cache.update(*ent, obj.bounding_box());
    }
}

struct LocationChangesSender(Sender<Event>);

impl EventSender for LocationChangesSender {
    fn send(&self, event: Event) -> bool { self.0.send(event).is_ok() }
}
struct LocationChangesReceiver(Receiver<Event>);

#[cfg(test)]
mod tests {
    use std::collections::HashSet;

    use arcs_core::{algorithms::Translate, primitives::Line, Point, Vector};
    use euclid::{Point2D, Size2D};
    use legion::Schedule;

    use super::*;

    fn init() -> (World, Resources, Schedule) {
        let mut world = World::default();

        let mut resources = Resources::default();
        init_resources(&mut resources, &mut world);

        let mut builder = Schedule::builder();
        register_maintenance_systems(&mut builder);
        let schedule = builder.build();

        (world, resources, schedule)
    }

    #[test]
    fn adding_objects_updates_cache() {
        let (mut world, mut res, mut schedule) = init();

        assert!(
            res.get::<LocationCache>().unwrap().is_empty(),
            "The location cache is empty to start with"
        );

        // add something to the world
        let ent = world.push((GraphicalObject::Line(Line::default()),));

        // and run our maintenance tasks
        schedule.execute(&mut world, &mut res);

        assert_eq!(
            (ent, Rect::zero()),
            res.get::<LocationCache>().unwrap().regions[0].clone(),
            "The location cache should now have one item"
        );
    }

    #[test]
    fn changing_an_object_updates_cache() {
        let (mut world, mut res, mut schedule) = init();

        // add something to the world
        let ent = world.push((GraphicalObject::Line(Line::default()),));
        // and make sure our cache is populated
        res.get_mut::<LocationCache>()
            .unwrap()
            .update(ent, Rect::zero());

        // now change something
        <&mut GraphicalObject>::query()
            .get_mut(&mut world, ent)
            .unwrap()
            .translate(Vector::new(100.0, 0.0));

        // and run our maintenance tasks
        schedule.execute(&mut world, &mut res);

        assert_eq!(
            (
                ent,
                Rect::new(Point::new(100.0, 0.0), Size2D::new(0.0, 0.0))
            ),
            res.get::<LocationCache>().unwrap().regions[0].clone(),
            "The location cache should now have one item"
        );
    }

    #[test]
    fn removing_objects_updates_cache() {
        let (mut world, mut res, mut schedule) = init();
        // add something to the world
        let ent = world.push((GraphicalObject::Line(Line::default()),));
        // and run our maintenance tasks
        schedule.execute(&mut world, &mut res);

        // now remove the item
        world.remove(ent);

        // Make sure we update the cache again
        schedule.execute(&mut world, &mut res);

        assert!(
            res.get::<LocationCache>().unwrap().is_empty(),
            "The location cache should now be empty"
        );
    }

    #[test]
    fn find_items_which_are_within_a_region() {
        let _ = tracing_subscriber::fmt()
            .json()
            .with_test_writer()
            .try_init();
        let mut world = World::default();
        let mut cache = LocationCache::default();

        let mut add = |start, end| {
            let obj = GraphicalObject::from(Line::new(start, end));
            let bounds = obj.bounding_box();
            let entity = world.push((obj,));
            cache.update(entity, bounds);
            entity
        };

        let region =
            Rect::new(Point2D::new(0.0, 0.0), Size2D::new(100.0, 100.0));

        let totally_within =
            add(Point2D::new(10.0, 10.0), Point2D::new(20.0, 20.0));
        let totally_outside =
            add(Point2D::new(-1000.0, -50.0), Point2D::new(-500.0, -500.0));
        let partially_within =
            add(Point2D::new(50.0, 50.0), Point2D::new(1000.0, 50.0));
        let overlapping_bounds_but_not_within =
            add(Point2D::new(90.0, 110.0), Point2D::new(115.0, 90.0));

        let got: HashSet<_> = cache
            .items_within_region(&world, region)
            .map(|(ent, _)| ent)
            .collect();

        assert!(got.contains(&totally_within));
        assert!(!got.contains(&totally_outside));
        assert!(got.contains(&partially_within));
        assert!(!got.contains(&overlapping_bounds_but_not_within));
    }
}
