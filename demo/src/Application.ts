import { Screen, DrawingDatabase, Coordinate as ArcsCoordinate } from "arcs";
import { AddLineMode, SelectMode, Mode, Transition } from "./modes";
import { BACKGROUND } from "./styles";

export type Coordinate = { x: number, y: number };

const PERFORMANCE_TIMING = false;

export default class Application {
    public currentMode: Mode;
    public canvas: HTMLCanvasElement;
    public cursorLocation: Coordinate;
    public drawing: DrawingDatabase;
    public screen: Screen;

    public onModeChanged?: (a: Application) => void;

    constructor(canvas: HTMLCanvasElement) {
        this.canvas = canvas;
        this.drawing = new DrawingDatabase();
        this.screen = new Screen(this.drawing);
        this.screen.set_background_colour(this.drawing, BACKGROUND);
        this.currentMode = new SelectMode();
        this.cursorLocation = { x: 0, y: 0 };

        this.screen = new Screen(this.drawing);
        this.screen.set_background_colour(this.drawing, "#eeeeee");
    }

    onMouseDown(e: MouseEvent) {
        this.updateCursorLocation(e);
        const t = this.currentMode.onMouseDown(e, this);
        this.handleTransition(t);
    }

    onMouseUp(e: MouseEvent) {
        this.updateCursorLocation(e);
        const t = this.currentMode.onMouseUp(e, this);
        this.handleTransition(t);
    }

    onMouseMove(e: MouseEvent) {
        this.updateCursorLocation(e);
        const t = this.currentMode.onMouseMove(e, this);
        this.handleTransition(t);
    }

    onScroll(e: WheelEvent) {
        this.updateCursorLocation(e);
        const t = this.currentMode.onScroll(e, this);
        this.handleTransition(t);
    }

    onKeyDown(e: KeyboardEvent) {
        if (e.key == " ") {
            // Dump out the drawing when the space bar is pressed (for debugging)
            console.log(this.drawing.toJSON());
        } else if (e.key == "1" && !(this.currentMode instanceof SelectMode)) {
            this.changeMode(new SelectMode());
        } else if (e.key == "2" && !(this.currentMode instanceof AddLineMode)) {
            this.changeMode(new AddLineMode());
        } else {
            const t = this.currentMode.onKeyDown(e, this);
            this.handleTransition(t);
        }
    }

    /**
     * Ask the application to re-paint the canvas.
     */
    requestRender() {
        this.timeit("render", () => {
            this.drawing.maintain();
            this.screen.render(this.drawing, this.canvas);
        });
    }

    changeMode(newMode: Mode) {
        this.currentMode.onExit(this);
        this.currentMode = newMode;
        this.currentMode.onEnter(this);

        this.onModeChanged?.(this);
    }

    entitiesWithinRegion(firstPoint: Coordinate, secondPoint: Coordinate): BigInt64Array {
        const first = new ArcsCoordinate(firstPoint.x, firstPoint.y);
        const second = new ArcsCoordinate(secondPoint.x, secondPoint.y);
        return this.drawing.entities_within_region(first, second);
    }

    private handleTransition(transition: Transition) {
        if (transition instanceof Mode) {
            this.changeMode(transition);
        }
    }

    private updateCursorLocation(e: MouseEvent) {
        const cursor = this.screen.to_drawing_coordinates(e.offsetX,
            e.offsetY, this.canvas.width, this.canvas.height, this.drawing);
        this.cursorLocation = { x: cursor.x, y: cursor.y };
        cursor.free();
    }

    private timeit<T>(label: string, f: () => T): T {
        if (PERFORMANCE_TIMING) {
            try {
                console.time("render");
                return f();
            } finally {
                console.timeEnd("render");
            }
        } else {
            return f();
        }
    }
}
