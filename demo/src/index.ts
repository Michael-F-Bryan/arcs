import Application from "./Application";
import { AddLineMode, SelectMode, Mode } from "./modes";
import { build_info } from "arcs";

interface ModeConstructor {
    new(): Mode;
}

type ButtonInfo = { cls: ModeConstructor, id: string };

const modeButtons: ButtonInfo[] = [
    { cls: SelectMode, id: "select-mode" },
    { cls: AddLineMode, id: "add-line-mode" },
];

onLoad(() => {
    const canvas = document.getElementById("canvas") as HTMLCanvasElement;
    const app = new Application(canvas);

    canvas.addEventListener("mousemove", e => app.onMouseMove(e));
    canvas.addEventListener("mousemove", () => {
        const { x, y } = app.cursorLocation;

        document.getElementById("cursor-x").innerHTML = (Math.round(x * 100) / 100).toString();
        document.getElementById("cursor-y").innerHTML = (Math.round(y * 100) / 100).toString();
    });
    canvas.addEventListener("mousedown", e => app.onMouseDown(e));
    canvas.addEventListener("mouseup", e => app.onMouseUp(e));
    canvas.addEventListener("wheel", e => {
        e.preventDefault();
        app.onScroll(e);
    });
    document.addEventListener("keydown", e => app.onKeyDown(e));

    document.getElementById("brand").addEventListener("click", () => {
        const info = JSON.stringify(build_info(), null, 2);
        alert(info);
    });

    // make sure the canvas resizes whenever the window changes (e.g. because
    // you opened dev tools)
    fitToContainer(canvas);
    window.addEventListener("resize", () => {
        fitToContainer(canvas);
        app.requestRender();
    });

    modeButtons.forEach(info => {
        const { id, cls } = info;
        const listener = () => app.changeMode(new cls());
        document.getElementById(id).addEventListener("click", listener);
    });
    onCurrentModeChanged(app);
    app.onModeChanged = onCurrentModeChanged;

    app.requestRender();
});

function onCurrentModeChanged(app: Application) {
    modeButtons.forEach(info => {
        const { id, cls } = info;
        const button = document.getElementById(id);

        if (app.currentMode instanceof cls) {
            button.classList.remove("default");
            button.classList.add("active");
        } else {
            button.classList.remove("active");
            button.classList.add("default");
        }
    });
}

function fitToContainer(canvas: HTMLCanvasElement) {
    // TODO: This kinda half works, but will slowly get wider/taller every time
    // we resize.
    canvas.style.width = "100%";
    canvas.style.height = "100%";
    canvas.width = canvas.offsetWidth;
    canvas.height = canvas.offsetHeight;
}


function onLoad(cb: () => void) {
    if (document.readyState !== "loading") {
        cb();
    } else {
        document.addEventListener('DOMContentLoaded', cb);
    }
}
