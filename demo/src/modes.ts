import { DrawingDatabase, Line, Screen, Coordinate as ArcsCoordinate, LineStyle, GraphicalObjectBuilder, Rect } from "arcs";
import { Coordinate } from "./Application";
import { LINE_COLOUR, PENDING_LINE_COLOUR } from "./styles";

function coord({ x, y }: Coordinate): ArcsCoordinate {
    return new ArcsCoordinate(x, y);
}

export interface Context {
    readonly canvas: HTMLCanvasElement;
    readonly cursorLocation: Coordinate;
    readonly screen: Screen;
    readonly drawing: DrawingDatabase;

    requestRender(): void;
    entitiesWithinRegion(firstPoint: Coordinate, secondPoint: Coordinate): BigInt64Array;
}

export type Transition = void | Mode;

export abstract class Mode {
    name() {
        return this.constructor.name;
    }

    onEnter(ctx: Context) { }

    onExit(ctx: Context) { }

    onMouseDown(e: MouseEvent, ctx: Context): Transition { }

    onMouseUp(e: MouseEvent, ctx: Context): Transition { }

    onMouseMove(e: MouseEvent, ctx: Context): Transition {
        if (panning(e)) {
            // dragging with the middle mouse button held.

            // Note: Negate `x` so the screen moves with the mouse, but leave
            // `y` as-is because the origin is the top-left in screen
            // coordinates but bottom-right in world coordinates. These fiddly
            // conversions will be handled properly by a transform matrix later
            // on, but for now let's just do it manually.
            const dx = -e.movementX;
            const dy = e.movementY;
            ctx.screen.pan(dx, dy, ctx.drawing);
            ctx.requestRender();
        }
    }

    onScroll(e: WheelEvent, ctx: Context): Transition {
        ctx.screen.zoom(e.deltaY >= 0 ? 0.9 : 1.1, ctx.drawing);
        ctx.requestRender();
    }

    onKeyDown(e: KeyboardEvent, ctx: Context): Transition {
        if (e.key == "Escape" && !(this instanceof SelectMode)) {
            return new SelectMode();
        }
    }
}

export class SelectMode extends Mode {
    pending?: { start: Coordinate, indicator: BigInt };

    onMouseDown(e: MouseEvent, ctx: Context) {
        if (this.pending) {
            // we're clicking the second spot.
            const { start, indicator } = this.pending;
            this.pending = undefined;
            ctx.drawing.remove(indicator);
            ctx.drawing.clear_selection();

            const second = ctx.cursorLocation;
            const entities = ctx.entitiesWithinRegion(start, second);

            entities.forEach(ent => {
                // Note: we've already deleted the indicator rectangle
                if (ent !== indicator) { ctx.drawing.select(ent); }
            });
            ctx.requestRender();
        } else {
            const { x, y } = ctx.cursorLocation;
            const entity = GraphicalObjectBuilder.rect(new Rect(new ArcsCoordinate(x, y), 0, 0))
                .build(ctx.drawing);

            this.pending = { start: ctx.cursorLocation, indicator: entity };
            ctx.requestRender();
        }
    }

    onMouseMove(e: MouseEvent, ctx: Context) {
        if (this.pending) {
            const { start: { x, y }, indicator: entity } = this.pending;
            const width = ctx.cursorLocation.x - x;
            const height = y - ctx.cursorLocation.y;

            ctx.drawing.update_rect(entity, new Rect(new ArcsCoordinate(x, y), width, height));
            ctx.requestRender();
        }
    }

    onKeyDown(e: KeyboardEvent, ctx: Context) {
        if (e.key === "Escape") {
            ctx.drawing.clear_selection();

            if (this.pending) {
                ctx.drawing.remove(this.pending.indicator);
                this.pending = undefined;
            }

            ctx.requestRender();
        }
    }

    onExit(ctx: Context) {
        if (this.pending) {
            ctx.drawing.remove(this.pending.indicator);
            this.pending = undefined;
            ctx.requestRender();
        }
    }
}


/**
 * A mode used to add lines to the drawing.
 */
export class AddLineMode extends Mode {
    pending?: { start: Coordinate, entity: BigInt };

    onMouseDown(e: MouseEvent, ctx: Context) {
        if (this.pending) {
            // we've clicked a second place - add the line to the drawing proper
            const start = coord(this.pending.start);
            const end = coord(ctx.cursorLocation);
            const builder = GraphicalObjectBuilder.line(new Line(start, end));
            builder.override_style(new LineStyle(LINE_COLOUR, 1, true));
            builder.build(ctx.drawing);

            // and remove the temporary one
            ctx.drawing.remove(this.pending.entity);
            this.pending = undefined;
        } else {
            // this is the first time we've clicked - start drawing a temporary
            // line so the user sees what they're doing.
            const start = coord(ctx.cursorLocation);
            const end = coord(ctx.cursorLocation);
            const builder = GraphicalObjectBuilder.line(new Line(start, end));
            builder.override_style(new LineStyle(PENDING_LINE_COLOUR, 1, true))

            this.pending = {
                start: ctx.cursorLocation,
                entity: builder.build(ctx.drawing),
            };
        }

        ctx.requestRender();
    }

    onMouseMove(e: MouseEvent, ctx: Context) {
        if (this.pending) {
            const { start, entity } = this.pending;
            ctx.drawing.update_line(entity, new Line(coord(start), coord(ctx.cursorLocation)));

            ctx.requestRender();
        }
    }

    onExit(ctx: Context) {
        if (this.pending) {
            // Make sure we don't leave half-finished lines lying around
            ctx.drawing.remove(this.pending.entity);
            this.pending = undefined;
            ctx.requestRender();
        }
    }
}

function panning(e: MouseEvent): boolean {
    return e.buttons === 4 || e.shiftKey;
}
