# Arcs Browser Demo

A demo for playing around with `arcs` in the browser.

## Getting Started

Before going any further, make sure you have followed the instructions from
the `wasm/` folder and compiled the WebAssembly bindings.

You will also need to a JavaScript package manager like
[`npm`](https://www.npmjs.com/get-npm) or
[`yarn`](https://yarnpkg.com/getting-started/install)).

Next, install the project's dependencies (include the WebAssembly bindings)...

```console
$ yarn install
[1/4] Resolving packages...
[2/4] Fetching packages...
[3/4] Linking dependencies...
[4/4] Building fresh packages...
success Saved lockfile.
Done in 1.21s.
```

... and bundle everything together with `webpack`.

```console
$ yarn build
 webpack --config webpack.config.js
 Hash: e2d297ee752218d5f687
 Version: webpack 4.46.0
 Time: 227ms
 Built at: 2021-01-24 5:38:27 AM
                            Asset       Size  Chunks                         Chunk Names
                   0.bootstrap.js   30.3 KiB       0  [emitted]
                   1.bootstrap.js   5.06 KiB       1  [emitted]
 5708011b09ee6559ec81.module.wasm   80.8 KiB       0  [emitted] [immutable]
                     bootstrap.js   14.6 KiB    main  [emitted]              main
                       index.html  367 bytes          [emitted]
 Entrypoint main = bootstrap.js
 [./bootstrap.js] 279 bytes {main} [built]
 [./index.js] 4.25 KiB {1} [built]
 [./node_modules/webpack/buildin/harmony-module.js] (webpack)/buildin/harmony-module.js 573 bytes {0} [built]
     + 3 hidden modules
 Done in 0.63s.
```

You can also use the `webpack` dev server when hacking on the demo itself.

```console
$ yarn start
 $ webpack-dev-server
 ℹ ｢wds｣: Project is running at http://localhost:8081/
 ℹ ｢wds｣: webpack output is served from /
 ℹ ｢wds｣: Content not from webpack is served from /home/michael/Documents/arcs/demo
 ℹ ｢wdm｣: Hash: 65aa9e4ab97a69172f1d
...
 ℹ ｢wdm｣: Compiled successfully.
```
