# Arcs WebAssembly Bindings

WebAssembly interface to the `arcs` CAD engine.

## Getting Started

To use these bindings, you will need to install:

- [Rust](https://rustup.rs/)
- [`wasm-pack`](https://github.com/rustwasm/wasm-pack) (`cargo install wasm-pack`)

Once you have installed the dependencies, `cd` into this directory and run the
following:

```console
$ wasm-pack build --out-name arcs
[INFO]: Checking for the Wasm target...
[INFO]: Compiling to Wasm...
   Compiling arcs-wasm v0.1.0 (./wasm)
    Finished release [optimized] target(s) in 0.15s
[INFO]: Installing wasm-bindgen...
[INFO]: Optimizing wasm binaries with `wasm-opt`...
[INFO]: :-) Done in 0.61s
[INFO]: :-) Your wasm pkg is ready to publish at ./wasm/pkg.
```

This will compile the project to WebAssembly, generate some JavaScript shims
for making the JS-WASM interop easier, and emit a TypeScript declarations
file.

```console
$ ls pkg
arcs.d.ts  arcs.js  arcs_bg.js  arcs_bg.wasm  arcs_bg.wasm.d.ts  package.json  README.md
```

You can add the generated code to your `package.json` as a local dependency.

```json
{
  "dependencies": {
    "arcs": "file:../path/to/wasm/pkg"
  }
}
```

See the demo application in the `demo/` directory for how these bindings can be
used in the browser.
