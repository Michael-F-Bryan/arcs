use legion::Entity;
use std::convert::TryInto;

/// When compiling to WebAssembly, *something* tries to import the `now()`
/// function from `env`. Webpack then fails to bundle everything together
/// because no such export is available.
///
/// By exporting our own `now()` function and overriding which section it is
/// placed in, we can fulfill our own dependency. This is a hack.
#[link_section = "env"]
#[no_mangle]
#[doc(hidden)]
pub extern "C" fn now() -> f64 { panic!() }

pub(crate) fn ent_to_int(entity: Entity) -> i64 {
    // Internally, an `Entity` is defined as a
    //
    // ```rust
    // #[repr(transparent)]
    // struct Entity(u64);
    // ```
    //
    // That means it is identical in memory to a `u64`. However, `legion`
    // doesn't want to expose any ways of inspecting the integer inside an
    // `Entity` because then downstream users may begin to rely on it or
    // accidentally break things in weird and wonderful ways.
    //
    // We would very much like to pass around integers because it makes interop
    // considerably easier. For example, if a function accepts arguments by
    // value, `wasm-bindgen` will generate code which unconditionally moves the
    // arguments (invalidating the JavaScript wrapper)... Regardless of whether
    // the argument is `Copy` or not. That means if we gave a plain `Entity` to
    // JavaScript it'd only be able to use that entity once.

    // Safety: The #[repr(transparent)] means this is sound, and if the backing
    // field changes later on we'll get a compilation error due to mismatched
    // sizes. All we're doing is violating Rust's privacy rules.

    let unsigned: u64 = unsafe { std::mem::transmute(entity) };

    unsigned
        .try_into()
        .expect("Overflow converting the entity to an integer")
}

pub(crate) fn int_to_ent(raw: i64) -> Entity {
    let unsigned: u64 = raw
        .try_into()
        .expect("Overflow converting the integer to an entity");

    unsafe { std::mem::transmute(unsigned) }
}
