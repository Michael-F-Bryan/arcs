//! WebAssembly bindings to `arcs`.

#![deny(
    missing_debug_implementations,
    missing_copy_implementations,
    rustdoc::broken_intra_doc_links
)]

mod drawing_database;
pub mod entities;
mod screen;
mod utils;

pub use drawing_database::DrawingDatabase;
pub use screen::Screen;
pub use utils::*;

use wasm_bindgen::{prelude::wasm_bindgen, JsValue};

/// A function called automatically when the WebAssembly module is loaded,
/// letting us automatically initialize things.
#[wasm_bindgen(start)]
pub fn on_load() {
    // Gives us nice panic messages.
    console_error_panic_hook::set_once();
}

/// Get information about how this package was compiled.
#[wasm_bindgen]
pub fn build_info() -> Result<JsValue, JsValue> {
    ::build_info::build_info!(fn info);

    JsValue::from_serde(info()).map_err(|e| JsValue::from(e.to_string()))
}
