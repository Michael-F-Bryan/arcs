//! Equivalents of our space-aware types which can be passed to JavaScript.

use arcs::{
    algorithms::Length,
    rendering::{Dimension, GraphicalObject},
};
use euclid::Point2D;
use piet::Color;
use wasm_bindgen::{prelude::wasm_bindgen, JsValue};

/// A 2D coordinate.
#[wasm_bindgen(inspectable)]
#[derive(
    Debug, Default, Copy, Clone, PartialEq, serde::Serialize, serde::Deserialize,
)]
pub struct Coordinate {
    pub x: f64,
    pub y: f64,
}

#[wasm_bindgen]
impl Coordinate {
    #[wasm_bindgen(constructor)]
    pub fn new(x: f64, y: f64) -> Self { Coordinate { x, y } }
}

impl<S> From<Coordinate> for Point2D<f64, S> {
    fn from(c: Coordinate) -> Self {
        let Coordinate { x, y } = c;
        Point2D::new(x, y)
    }
}

impl<S> From<Point2D<f64, S>> for Coordinate {
    fn from(c: Point2D<f64, S>) -> Self {
        let Point2D { x, y, .. } = c;
        Coordinate::new(x, y)
    }
}

/// A finite line.
///
/// Note: All coordinates are in *Drawing Space*.
#[wasm_bindgen(inspectable)]
#[derive(
    Debug, Default, Copy, Clone, PartialEq, serde::Serialize, serde::Deserialize,
)]
pub struct Line {
    pub start: Coordinate,
    pub end: Coordinate,
}

#[wasm_bindgen]
impl Line {
    #[wasm_bindgen(constructor)]
    pub fn new(start: Coordinate, end: Coordinate) -> Self {
        Line { start, end }
    }
}

impl<S> From<Line> for arcs::primitives::Line<S> {
    fn from(line: Line) -> Self {
        let Line { start, end } = line;
        arcs::primitives::Line::new(start.into(), end.into())
    }
}

impl From<Line> for GraphicalObject {
    fn from(line: Line) -> Self { GraphicalObject::Line(line.into()) }
}

#[wasm_bindgen(inspectable)]
#[derive(
    Debug, Default, Copy, Clone, PartialEq, serde::Serialize, serde::Deserialize,
)]
pub struct Rect {
    pub centre: Coordinate,
    pub width: f64,
    pub height: f64,
}

#[wasm_bindgen]
impl Rect {
    #[wasm_bindgen(constructor)]
    pub fn new(centre: Coordinate, width: f64, height: f64) -> Self {
        Rect {
            centre,
            width,
            height,
        }
    }
}

impl<S> From<Rect> for euclid::Rect<f64, S> {
    fn from(rect: Rect) -> Self {
        let Rect {
            centre,
            width,
            height,
        } = rect;

        let size = euclid::Size2D::new(width, height);

        euclid::Rect::new(centre.into(), size)
    }
}

impl From<Rect> for GraphicalObject {
    fn from(rect: Rect) -> Self { GraphicalObject::Rectangle(rect.into()) }
}

#[wasm_bindgen]
#[derive(Debug, Clone)]
pub struct LineStyle(pub(crate) arcs::rendering::LineStyle);

#[wasm_bindgen]
impl LineStyle {
    #[wasm_bindgen(constructor)]
    pub fn new(
        stroke: &str,
        width: f64,
        width_in_pixels: bool,
    ) -> Result<LineStyle, JsValue> {
        let stroke = Color::from_hex_str(stroke)
            .map_err(|e| JsValue::from(e.to_string()))?;

        let width = if width_in_pixels {
            Dimension::Pixels(width)
        } else {
            Dimension::DrawingUnits(Length::new(width))
        };

        Ok(LineStyle(arcs::rendering::LineStyle { stroke, width }))
    }
}

impl From<LineStyle> for arcs::rendering::LineStyle {
    fn from(s: LineStyle) -> arcs::rendering::LineStyle { s.0 }
}
