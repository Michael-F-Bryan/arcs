use crate::entities::Coordinate;
use arcs::{
    primitives::Line,
    rendering::{GraphicalObject, LineStyle, LocationCache, Selected},
    DrawingSpace, Overridable,
};
use euclid::Rect;
use legion::{
    serialize::Registry,
    systems::{CommandBuffer, Schedule},
    Entity, IntoQuery, Resources, World,
};
use std::fmt::{self, Debug, Formatter};
use wasm_bindgen::{prelude::wasm_bindgen, JsValue};

macro_rules! registry {
    ($( $ty:ty ),* $(,)?) => {{
        let mut registry = Registry::<String>::default();

        $(
            registry.register::<$ty>(std::any::type_name::<$ty>().into());
        )*

        registry
    }};
}

/// A database containing everything in a drawing.
#[wasm_bindgen]
pub struct DrawingDatabase {
    world: World,
    registry: Registry<String>,
    schedule: Schedule,
    resources: Resources,
}

/// Functionality for managing the world.
#[wasm_bindgen]
impl DrawingDatabase {
    /// Create a new [`DrawingDatabase`].
    #[wasm_bindgen(constructor)]
    pub fn new() -> Self {
        let registry = registry!(
            Line<DrawingSpace>,
            arcs::rendering::LineStyle,
            Overridable<LineStyle>,
            Selected,
        );
        let mut world = World::default();

        let mut resources = Resources::default();
        arcs::rendering::init_resources(&mut resources, &mut world);

        let mut builder = Schedule::builder();
        arcs::rendering::register_maintenance_systems(&mut builder);
        let schedule = builder.build();

        DrawingDatabase {
            world,
            registry,
            schedule,
            resources,
        }
    }

    /// Delete everything from the [`DrawingDatabase`].
    pub fn clear(&mut self) { self.world.clear(); }

    /// Execute maintenance tasks like updating caches.
    ///
    /// This method should be called before rendering.
    pub fn maintain(&mut self) {
        self.schedule.execute(&mut self.world, &mut self.resources);
    }

    /// Removes an entity from the [`World`].
    pub fn remove(&mut self, entity: i64) {
        let entity = crate::int_to_ent(entity);
        self.world.remove(entity);
    }

    /// Serialize the world as JSON.
    #[wasm_bindgen(js_name = "toJSON")]
    pub fn to_json(&self) -> JsValue {
        let ser = self.world.as_serializable(
            legion::component::<Line<DrawingSpace>>(),
            &self.registry,
        );
        JsValue::from_serde(&ser).unwrap()
    }

    /// Get a list of all the entities within a region of the drawing.
    pub fn entities_within_region(
        &self,
        first_corner: Coordinate,
        second_corner: Coordinate,
    ) -> Box<[i64]> {
        let region =
            Rect::from_points(&[first_corner.into(), second_corner.into()]);

        let cache = self
            .resources()
            .get::<LocationCache>()
            .expect("The LocationCache should have been added on startup");

        cache
            .items_within_region(self.world(), region)
            .map(|(ent, _)| crate::ent_to_int(ent))
            .collect()
    }
}

impl Debug for DrawingDatabase {
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        let DrawingDatabase {
            world,
            resources: _,
            schedule: _,
            registry: _,
        } = self;

        f.debug_struct("DrawingDatabase")
            .field("world", world)
            .finish()
    }
}

impl DrawingDatabase {
    pub fn world(&self) -> &World { &self.world }

    pub fn world_mut(&mut self) -> &mut World { &mut self.world }

    pub fn resources(&self) -> &Resources { &self.resources }

    pub fn resources_mut(&mut self) -> &mut Resources { &mut self.resources }

    fn update_graphical_object<G>(
        &mut self,
        entity: i64,
        obj: G,
    ) -> Result<(), JsValue>
    where
        G: Into<GraphicalObject>,
    {
        let entity = crate::int_to_ent(entity);

        let mut query = <&mut GraphicalObject>::query();
        let place = query
            .get_mut(self.world_mut(), entity)
            .map_err(|e| JsValue::from(e.to_string()))?;

        *place = obj.into();

        Ok(())
    }
}

/// Entity properties.
#[wasm_bindgen]
impl DrawingDatabase {
    pub fn update_line(
        &mut self,
        entity: i64,
        line: crate::entities::Line,
    ) -> Result<(), JsValue> {
        self.update_graphical_object(entity, line)
    }

    pub fn update_rect(
        &mut self,
        entity: i64,
        rect: crate::entities::Rect,
    ) -> Result<(), JsValue> {
        self.update_graphical_object(entity, rect)
    }

    /// Select a single entity.
    pub fn select(&mut self, entity: i64) -> Result<(), JsValue> {
        let entity = crate::int_to_ent(entity);

        self.world
            .entry(entity)
            .ok_or_else(|| JsValue::from_str("No such entity"))?
            .add_component(Selected);

        Ok(())
    }

    /// Select a single entity.
    pub fn unselect(&mut self, entity: i64) -> Result<(), JsValue> {
        let entity = crate::int_to_ent(entity);

        self.world
            .entry(entity)
            .ok_or_else(|| JsValue::from_str("No such entity"))?
            .remove_component::<Selected>();

        Ok(())
    }

    /// Get a list of all the selected entities.
    pub fn get_selected(&self) -> Box<[i64]> {
        let mut entities = Vec::new();

        <(Entity, &Selected)>::query().for_each(self.world(), |(ent, _)| {
            entities.push(crate::ent_to_int(*ent))
        });

        entities.into_boxed_slice()
    }

    pub fn clear_selection(&mut self) {
        let mut cmd = CommandBuffer::new(self.world());

        <(Entity, &Selected)>::query().for_each(self.world(), |(ent, _)| {
            cmd.remove_component::<Selected>(*ent);
        });

        cmd.flush(self.world_mut());
    }
}

#[wasm_bindgen]
#[derive(Debug, arcs_wasm_macros::EntityBuilder)]
pub struct GraphicalObjectBuilder {
    #[builder(required)]
    obj: GraphicalObject,
    #[builder(js = crate::entities::LineStyle)]
    style: Overridable<LineStyle>,
}

impl GraphicalObjectBuilder {
    pub fn new(obj: impl Into<GraphicalObject>) -> Self {
        GraphicalObjectBuilder {
            obj: obj.into(),
            style: Overridable::Inherited,
        }
    }
}

#[wasm_bindgen]
impl GraphicalObjectBuilder {
    pub fn line(line: crate::entities::Line) -> Self {
        GraphicalObjectBuilder::new(GraphicalObject::Line(line.into()))
    }

    pub fn rect(rect: crate::entities::Rect) -> Self {
        GraphicalObjectBuilder::new(GraphicalObject::Rectangle(rect.into()))
    }
}
