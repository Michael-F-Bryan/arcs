use crate::{entities::Coordinate, DrawingDatabase};
use arcs::{
    rendering::{Background, NormalStyles, SelectedStyles, Viewport},
    CanvasSpace, DrawingSpace,
};
use euclid::{Point2D, Scale, Size2D, Vector2D};
use legion::{Entity, IntoQuery};
use piet::{Color, RenderContext};
use piet_web::WebRenderContext;
use wasm_bindgen::{prelude::wasm_bindgen, JsCast, JsValue};
use web_sys::{CanvasRenderingContext2d, HtmlCanvasElement};

/// The CAD window that everything is rendered to.
///
/// # A Note on Conventions
///
/// There are a couple coordinate systems going on in a CAD application,
/// namely:
///
/// - [*Canvas Space:*][arcs::CanvasSpace] the location of a pixel on the HTML
///   canvas (graphical coordinates)
/// - [*Drawing Space:*][arcs::DrawingSpace] a location in the drawing
///   (cartesian coordinates)
///
/// To convert between *Canvas* and *Drawing* coordinates we need to:
///
/// - Flip vertically about the middle of the canvas
/// - Translate from the centre of the canvas to the `centre` of the
///   [`Viewport`]
/// - Scale to respect [`Viewport::pixels_per_drawing_unit`]
///
/// Internally, `arcs` uses the type system to make sure coordinate systems
/// don't get mixed up, but in JavaScript world you just need to remember.
///
/// [`Viewport::transform()`] will give you a transform matrix that handles
/// the conversion correctly.
#[wasm_bindgen]
#[derive(Debug, Clone, PartialEq)]
#[allow(missing_copy_implementations)]
pub struct Screen {
    /// The entity used to store the [`Screen`]'s components.
    entity: Entity,
}

#[wasm_bindgen]
impl Screen {
    #[wasm_bindgen(constructor)]
    pub fn new(drawing_database: &mut DrawingDatabase) -> Self {
        let entity = drawing_database.world_mut().push((
            SelectedStyles::default(),
            NormalStyles::default(),
            Viewport::default(),
            Background::default(),
        ));

        Screen { entity }
    }

    /// Render the visible parts of the drawing to a HTML canvas element.
    ///
    /// Note: It is up to the caller to clear the screen beforehand.
    pub fn render(
        &self,
        drawing_database: &DrawingDatabase,
        html_canvas: HtmlCanvasElement,
    ) -> Result<(), JsValue> {
        let context = html_canvas
            .get_context("2d")?
            .ok_or_else(|| {
                JsValue::from_str("Unable to get the 2D rendering context")
            })?
            .dyn_into::<CanvasRenderingContext2d>()?;

        let mut ctx =
            WebRenderContext::new(context, web_sys::window().unwrap());
        let height = html_canvas.height() as f64;
        let width = html_canvas.width() as f64;
        let canvas_dimensions = Size2D::new(width, height);
        let world = drawing_database.world();
        let resources = drawing_database.resources();

        arcs::rendering::render(
            self.entity,
            world,
            resources,
            canvas_dimensions,
            &mut ctx,
        );

        ctx.finish().map_err(|e| JsValue::from(e.to_string()))
    }

    /// Move the [`Viewport`] by a certain number of **pixels**.
    pub fn pan(
        &mut self,
        delta_x: f64,
        delta_y: f64,
        drawing_database: &mut DrawingDatabase,
    ) {
        let delta = Vector2D::<f64, CanvasSpace>::new(delta_x, delta_y);

        let viewport = self.viewport_mut(drawing_database);

        viewport.centre += viewport
            .pixels_per_drawing_unit
            .inverse()
            .transform_vector(delta);
    }

    /// Convert from [`CanvasSpace`] to [`DrawingSpace`].
    pub fn to_drawing_coordinates(
        &self,
        x: f64,
        y: f64,
        canvas_width: f64,
        canvas_height: f64,
        drawing_database: &DrawingDatabase,
    ) -> Coordinate {
        let coord = Point2D::<f64, CanvasSpace>::new(x, y);
        let canvas_dimensions =
            Size2D::<f64, CanvasSpace>::new(canvas_width, canvas_height);

        self.viewport(drawing_database)
            .transform(canvas_dimensions)
            .transform_point(coord)
            .into()
    }

    /// Convert from [`DrawingSpace`] to [`CanvasSpace`].
    pub fn to_canvas_coordinates(
        &self,
        x: f64,
        y: f64,
        canvas_width: f64,
        canvas_height: f64,
        drawing_database: &DrawingDatabase,
    ) -> Coordinate {
        let coord = Point2D::<f64, DrawingSpace>::new(x, y);
        let canvas_dimensions =
            Size2D::<f64, CanvasSpace>::new(canvas_width, canvas_height);

        self.viewport(drawing_database)
            .transform(canvas_dimensions)
            .inverse()
            .unwrap()
            .transform_point(coord)
            .into()
    }

    pub fn zoom(
        &mut self,
        scale_factor: f64,
        drawing_database: &mut DrawingDatabase,
    ) {
        let viewport = self.viewport_mut(drawing_database);
        viewport.pixels_per_drawing_unit =
            viewport.pixels_per_drawing_unit * Scale::new(scale_factor);
    }

    /// Set the background colour using a CSS-style hex string (e.g.
    /// `#001484"`).
    pub fn set_background_colour(
        &self,
        drawing_database: &mut DrawingDatabase,
        hex: &str,
    ) -> Result<(), JsValue> {
        let background = <&mut Background>::query()
            .get_mut(drawing_database.world_mut(), self.entity)
            .map_err(|e| JsValue::from(e.to_string()))?;

        background.colour = Color::from_hex_str(hex)
            .map_err(|e| JsValue::from(e.to_string()))?;

        Ok(())
    }
}

impl Screen {
    pub fn viewport_mut<'world>(
        &mut self,
        drawing_database: &'world mut DrawingDatabase,
    ) -> &'world mut Viewport {
        <&mut Viewport>::query()
            .get_mut(drawing_database.world_mut(), self.entity)
            .expect("All Screens should have a viewport")
    }

    pub fn viewport<'world>(
        &self,
        drawing_database: &'world DrawingDatabase,
    ) -> &'world Viewport {
        <&Viewport>::query()
            .get(drawing_database.world(), self.entity)
            .expect("All Screens should have a viewport")
    }
}
