extern crate proc_macro;

use proc_macro2::{Ident, TokenStream};
use quote::quote;
use syn::{
    ext::IdentExt,
    parse::{Parse, ParseStream},
    parse_macro_input,
    punctuated::Punctuated,
    spanned::Spanned,
    token::Comma,
    Data, DataStruct, DeriveInput, Error, Fields, Token, Type, Visibility,
};

/// Create a builder type for initializing an entity with a set of components.
#[proc_macro_derive(EntityBuilder, attributes(builder))]
pub fn entity_builder(
    input: proc_macro::TokenStream,
) -> proc_macro::TokenStream {
    let input = parse_macro_input!(input as DeriveInput);

    if !input.generics.params.is_empty() {
        return Error::new(
            input.generics.span(),
            "Entity builders can't be generic",
        )
        .to_compile_error()
        .into();
    }

    let fields = match input.data {
        Data::Struct(DataStruct {
            fields: Fields::Named(named),
            ..
        }) => named,
        Data::Struct(DataStruct { .. }) => {
            return Error::new(
                input.span(),
                "This derive doesn't work with tuple or unit structs",
            )
            .to_compile_error()
            .into()
        },
        _ => {
            return Error::new(
                input.span(),
                "This derive only works on structs",
            )
            .to_compile_error()
            .into()
        },
    };

    let mut required_fields = Vec::new();
    let mut optional_fields = Vec::new();

    for field in fields.named {
        match Field::from_syn(field) {
            Ok((f, true)) => required_fields.push(f),
            Ok((f, false)) => optional_fields.push(f),
            Err(e) => return e.to_compile_error().into(),
        }
    }

    let DeriveInput { ident, vis, .. } = input;

    let methods = optional_fields.iter().map(|f| overridable_methods(&vis, f));

    let field_names: Vec<_> = required_fields
        .iter()
        .map(|f| &f.name)
        .chain(optional_fields.iter().map(|f| &f.name))
        .collect();
    let build = build_method(&vis, &ident, &field_names);

    quote!(
        #[wasm_bindgen]
        impl #ident {
            #( #methods )*

            #build
        }
    )
    .into()
}

fn build_method(
    vis: &Visibility,
    name: &Ident,
    field_names: &[&Ident],
) -> TokenStream {
    quote!(
        /// Create the entity, getting back a unique handle we can use when
        /// referring to it.
        #vis fn build(self, drawing_database: &mut DrawingDatabase) -> i64 {
            let #name { #( #field_names ),* } = self;

            let ent = drawing_database.world_mut().push(( #( #field_names ),* ));

            crate::ent_to_int(ent)
        }
    )
}

fn overridable_methods(vis: &Visibility, field: &Field) -> TokenStream {
    let Field { name, js_type, .. } = &field;
    let name_as_str = name.to_string();

    let setter_name =
        Ident::new(&format!("override_{}", field.name), field.name.span());
    let setter = quote!(
        /// Override the
        #[doc = #name_as_str]
        /// component.
        #vis fn #setter_name(&mut self, #name: #js_type) {
            self.#name = Overridable::Overridden(#name.into());
        }
    );

    let clear_name =
        Ident::new(&format!("clear_{}", field.name), field.name.span());

    let clear = quote!(
        /// Clear the
        #[doc = #name_as_str]
        /// component, resetting it to its default.
        #vis fn #clear_name(&mut self) {
            self.#name = Default::default();
        }
    );

    quote!(#setter #clear)
}

struct Field {
    name: Ident,
    js_type: Option<Type>,
}

impl Field {
    fn from_syn(f: syn::Field) -> Result<(Self, bool), Error> {
        let name = match f.ident {
            Some(n) => n,
            None => return Err(Error::new(f.span(), "Field should be named")),
        };

        let mut required = false;
        let mut js_type = None;

        for attr in &f.attrs {
            if !attr.path.is_ident("builder") {
                continue;
            }
            let metas: Punctuated<Meta, Comma> =
                attr.parse_args_with(Punctuated::parse_terminated)?;

            for meta in metas {
                match meta {
                    Meta::Ident(id) if id.to_string() == "required" => {
                        required = true
                    },
                    Meta::Ident(other_ident) => {
                        return Err(Error::new(other_ident.span(), format!("the only attribute valid in this position is `required` but found `{}`", other_ident)));
                    },
                    Meta::KeyValue { key, value }
                        if key.to_string() == "js" =>
                    {
                        js_type = Some(value);
                    }
                    Meta::KeyValue { key, .. } => {
                        return Err(Error::new(key.span(), format!("the only attribute valid in this position is `js = ...` but found `{} = ...`", key)));
                    },
                }
            }
        }

        if !required {
            // non-required fields should be some form of Overridable<T>.
            let is_overridable_t = match &f.ty {
                Type::Path(p) if !p.path.segments.is_empty() => {
                    let last_segment = p.path.segments.last().unwrap();
                    last_segment.ident.to_string() == "Overridable"
                        && matches!(
                            last_segment.arguments,
                            syn::PathArguments::AngleBracketed(_)
                        )
                },
                _ => false,
            };

            if !is_overridable_t {
                return Err(Error::new(f.ty.span(), "non-required components must be some form of `Overridable<T>`"));
            }
        }

        Ok((Field { name, js_type }, required))
    }
}

enum Meta {
    Ident(Ident),
    KeyValue { key: Ident, value: Type },
}

impl Parse for Meta {
    fn parse(input: ParseStream) -> Result<Self, Error> {
        if input.peek(Ident::peek_any) {
            if input.peek2(Token![=]) {
                let key = Ident::parse(input)?;
                let _ = <Token![=]>::parse(input)?;
                let value = Type::parse(input)?;

                return Ok(Meta::KeyValue { key, value });
            }

            Ident::parse(input).map(Meta::Ident)
        } else {
            Err(input.error("expected identifier or key-value pair"))
        }
    }
}
