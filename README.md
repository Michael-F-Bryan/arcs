# arcs

[![pipeline status](https://gitlab.com/Michael-F-Bryan/arcs/badges/master/pipeline.svg)](https://gitlab.com/Michael-F-Bryan/arcs/pipelines)

(**[API Docs](https://michael-f-bryan.gitlab.io/arcs/crate-docs/index.html) |
   [WebAssembly Bindings](https://michael-f-bryan.gitlab.io/arcs/arcs.wasm.zip) |
   [Online Demo](https://michael-f-bryan.gitlab.io/arcs/demo/index.html)**)

A Rust CAD System.

## Repository Layout

This repository has been split into several pieces based on functionality:

- `arcs/` - a façade crate which re-exports all `arcs` functionality under one
  name
- `core/` - lightweight crate containing core geometric primitives and
  algorithms
- `demo/` - a demo which shows off some of the engine's functionality and runs
  in the browser
- `render/` - data structures and algorithms used to render drawings to the
  screen.
- `wasm/` - WebAssembly bindings to `arcs` for use in the browser

## Support

The source code and documentation for this project is open source.

However, to help subsidise the large amount of time spent developing this
project, support will only be provided with a service agreement.

| Service         | Description                                        | Payment             |
| --------------- | -------------------------------------------------- | ------------------- |
| Basic           | Guaranteed response on the issue tracker           | yearly subscription |
| Feature Request | Request for features to be added to the project    | one-off payment     |
| Integration     | Assistance in integrating `arcs` with your product | one-off payment     |

## Development

During development I will typically have two terminals watching for changes
in the background.

The first terminal will be recompiling the WebAssembly demo using `wasm-pack`
and `webpack`:

```console
cargo watch --clear --ignore render/tests/snapshots \
        -s "wasm-pack build --dev --out-name arcs wasm" \
        -s "cd demo && yarn link arcs && yarn start"
```

The second terminal compiles all the packages to native code, generates API
docs, and runs the test suite.

```console
cargo watch --clear --ignore render/tests/snapshots --ignore wasm --ignore demo \
        -x "check --workspace" \
        -x "test --workspace" \
        -x "doc --document-private-items --workspace"
```

Both are intended to be run from the project root.

If starting from a fresh `git clone` you will need to tell the demo to `link`
to our WebAssembly bindings.

```console
wasm-pack build --dev --out-name arcs wasm && \
  pushd wasm/pkg && yarn link && popd && \
  pushd demo && yarn install && yarn link arcs && popd
```

Not using `yarn link` means you can make as many changes to the WebAssembly
bindings and do as many `wasm-pack` runs as you want, but the demo won't see
any changes. When you `yarn install` a `file:...` dependency, `yarn` will
**copy** the dependency into `node_modules/`.

Using `yarn link` will overwrite this copy with a symlink.

## Licensing

The source code for project is licensed under either of

 * Apache License, Version 2.0, ([LICENSE-APACHE](LICENSE-APACHE.md) or
   http://www.apache.org/licenses/LICENSE-2.0)
 * MIT license ([LICENSE-MIT](LICENSE-MIT.md) or
   http://opensource.org/licenses/MIT)

at your option.

It is recommended to always use [cargo-crev][crev] to verify the
trustworthiness of each of your dependencies, including this one.

### Contribution

Unless you explicitly state otherwise, any contribution intentionally
submitted for inclusion in the work by you, as defined in the Apache-2.0
license, shall be dual licensed as above, without any additional terms or
conditions.

The intent of this crate is to be free of soundness bugs. The developers will
do their best to avoid them, and welcome help in analysing and fixing them.

[API Docs]: https://michael-f-bryan.gitlab.io/arcs
[crev]: https://github.com/crev-dev/cargo-crev
